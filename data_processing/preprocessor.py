import os

import numpy as np 
import pandas as pd
from sklearn.linear_model import LinearRegression

class preprocessor:
    def __init__(
        self,
        data_path,
    ):

        self.data_path = data_path
        self.stations = None
        self.start_date = None
        self.end_date = None
        self.data = None
        self.verbose = None 
        
    def load_initial_data(self,
        station,
        start_date,
        end_date,
    ):
        dic = dict()
        strains = {
            "Eee+Enn": "areal_strain",
            "Eee-Enn": "differential_strain",
            "2Ene": "shear_strain",
        }
        for strain_type in strains.keys():
            all_years = []
            for year in range(start_date, end_date + 1):
                try:
                    year_path = (
                        self.data_path
                        + f"/{station}/{year}/{station}.{year}.xml."
                    )
                    final_path = year_path + strain_type + ".txt"
                    year_data = pd.read_csv(final_path, delimiter="\s+")
                    year_data = self._correct_data(year_data)
                    all_years.append(year_data)
                except:
                    continue
            dic[strains[strain_type]]=pd.concat(all_years)
        self.initial_data = dic
    
    def _flag_filter(self, df , name , year , interpolation_type) :
        data =df.copy() 
        index =df[df['strain_quality'] == 'b'].index.tolist()  
        index += df[df['strain_quality'] == 'm'].index.tolist()  
        index += df[abs(df[name]) >99999].index.tolist() 
        df.loc[ index , name] = np.nan
        if self.verbose : 
            number = df[name].isna().sum() 
            print(f'number of interpolated observation of {name} is {number} on {year}')
        if interpolation_type == 'linear' : 
            df[name] = df[name].interpolate() 
        elif interpolation_type == 'backward' : 
            df[name] = df[name].bfill()
        return df 
    
    def _correct_data(self, data , strain_name):
        transformed = data.copy()
        cols = transformed.columns
        transformed[strain_name] = (
            transformed[cols[4]]
            + transformed[cols[5]]
            - transformed[cols[7]]
            - transformed[cols[8]]
            - transformed[cols[9]]
        )
        transformed.date = pd.to_datetime(transformed.date)
        transformed.set_index("date", inplace=True)
        return transformed
    
    def _detrend(self , data , method , name) : 
        df = data.copy()
        if method == 'diff' : 
            new_name = '_'.join(name.split('_')[:-1]) + '_detrended_' + name.split('_')[-1]
            df[new_name] = df[name] - df[name].shift(1)
        elif method == 'per_of_change' : 
            new_name = '_'.join(name.split('_')[:-1]) + '_detrended_' + name.split('_')[-1]
            df[new_name] = (df[name] -df[name].shift(1))/df[name].shift(1) 
        elif method == 'linear_fitting' : 
            new_name = '_'.join(name.split('_')[:-1]) + '_detrended_' + name.split('_')[-1]
            series = df[name].values
            X = [i for i in range(0, len(series))]
            X = np.reshape(X, (len(X), 1))
            model = LinearRegression()
            model.fit(X, series)
            trend = model.predict(X)
            df[new_name] = [series[i]-trend[i] for i in range(0, len(series))]
        return df 
            
    def load(
        self,
        stations,
        start_date,
        end_date,
        flag_filter = None, 
        detrending = None , 
        verbose = False , 
        drop_na = False , 
    ):        
        self.verbose = verbose
        self.stations = stations
        self.start_date = start_date
        self.end_date = end_date
        starins = {
            "Eee+Enn": "areal_strain",
            "Eee-Enn": "differential_strain",
            "2Ene": "shear_strain",
        }
        all_satations = []
        for station in stations:
            all_years = []
            for year in range(start_date, end_date + 1):
                year_path = (
                    self.data_path
                    + f"/{station}/{year}/{station}.{year}.xml."
                )
                init = 0
                all_starins = []
                for strain_type in starins.keys():
                    
                    final_path = year_path + strain_type + ".txt"
                    year_data = pd.read_csv(final_path, delimiter="\s+")
                    year_data = self._correct_data(year_data , starins[strain_type] + "_" + station)
                    
                    if flag_filter is not None : 
                        year_data = self._flag_filter(year_data,starins[strain_type] + "_" + station , year ,flag_filter )
                    
                    if detrending is None : 
                        all_starins.append(year_data[starins[strain_type] + "_" + station])
                    else : 
                        year_data = self._detrend(year_data , detrending , starins[strain_type] + "_" + station)
                        cols = [starins[strain_type] + "_" + station , starins[strain_type] + "_detrended_" + station ] 
                        all_starins.append(year_data[cols])
                        
                all_starins = pd.concat(all_starins, axis=1)
                all_years.append(all_starins)
            data = pd.concat(all_years)
            all_satations.append(data)
        self.data = pd.concat(all_satations, axis=1)

    def meta_data(self):
        path_to_data = os.path.join(self.data_path, "Locations.csv")
        data = pd.read_csv(path_to_data)
        min_year = []
        max_year = []
        for station in data.Station:
            station_path = self.data_path + "/" + station
            years = []
            for year in os.listdir(station_path):
                if year[0] == "2":
                    years.append(year)
            min_year.append(min(years))
            max_year.append(max(years))
        data["start_date"] = min_year
        data["end_date"] = max_year
        return data
    
    def get_per_of_bad_obs_per_station(self) : 
        meta_data = self.meta_data() 
        mean_per_station = []
        for station in meta_data.Station.values : 
            station_per = []
            for year in range(2013, 2021): 
                try :  
                    year_path = (
                        self.data_path
                        + f"/{station}/{year}/{station}.{year}.xml."
                    )
                    final_path = year_path + "2Ene.txt"
                    df = pd.read_csv(final_path, delimiter="\s+")
                    index =df[df['strain_quality'] == 'b'].index.tolist()  
                    index += df[df['strain_quality'] == 'm'].index.tolist()  
                    station_per.append(len(index)/len(df))
                except : 
                    station_per.append(np.nan) 
                    print(f'station: {station} , year : {year}')

            mean_per_station.append(np.array(station_per).mean())
                
        meta_data['bad_obs_mean'] = mean_per_station 
        return meta_data