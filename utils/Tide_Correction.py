import math 
import pandas as pd 
import numpy as np 
import utide
import matplotlib.dates as mdates
import matplotlib.pyplot as plt
plt.style.use('fivethirtyeight')
plt.figure(figsize=(20,10))

from data_processing.preprocessor import *
from utils.Location_utils import *


def load(station,start_date=2018,end_date=2019):
    prep = preprocessor('/home/amine/University_Project/ml_geodetic_project/Data')
    prep.load_initial_data( station = station, 
               start_date = start_date, 
                end_date = end_date)
    areal=prep.initial_data['areal_strain']
    shear=prep.initial_data['shear_strain']
    differential=prep.initial_data['differential_strain']
    return areal,shear,differential

def Tidal_Correction(data,visualize=False):
    target=data.Final_Strain
    time = mdates.date2num(data.index)
    locations=pd.read_csv('/home/amine/University_Project/ml_geodetic_project/Data/Locations.csv')
    Lat=locations[locations.Station=='B009'].Latitude
    coef = utide.solve(time,target,
                   lat=Lat.values,
                   method='ols',trend=True,verbose=True,
                   conf_int='MC',white=False)
    tide = utide.reconstruct(time, coef)
    data['Tide_corrected_Strain']=target - tide.h
    
    if visualize == True:
        t = tide.t_mpl
        fig, (ax0, ax1, ax2) = plt.subplots(nrows=3, sharey=True, sharex=True,figsize=(20,10))

        ax0.plot(t, target, label=u'target', color='C0')
        ax1.plot(t, tide.h, label=u'Tide Fit', color='C1')
        ax2.plot(t, target - tide.h, label=u'Residual', color='C2')
        ax2.xaxis_date()
        fig.legend(ncol=3, loc='upper center')
        fig.autofmt_xdate()
    return data