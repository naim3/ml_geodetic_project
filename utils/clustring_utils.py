import math

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

from sklearn.preprocessing import MinMaxScaler


def plots(Series, namesofSeries, feat):
    fig, axs = plt.subplots(2, 4, figsize=(25, 12))
    fig.suptitle(feat)
    for i in range(2):
        for j in range(4):
            if i * 4 + j + 1 > len(Series):  # pass the others that we can't fill
                continue
            axs[i, j].plot(Series[i * 4 + j].values)
            axs[i, j].set_title(namesofSeries[i * 4 + j])
    plt.show()


def plot_histogram(Series, namesofSeries, feat):
    fig, axs = plt.subplots(2, 4, figsize=(25, 12))
    fig.suptitle("Histograms of " + feat)
    for i in range(2):
        for j in range(4):
            if i * 4 + j + 1 > len(Series):  # pass the others that we can't fill
                continue
            axs[i, j].hist(Series[i * 4 + j].values, bins=10)
            axs[i, j].set_title(namesofSeries[i * 4 + j] + "histogram")
    plt.show()


def get_series(data, feat, stations):
    Series = []
    namesofSeries = []
    for station in stations:
        Series.append(data[feat + "_" + station])
        namesofSeries.append(station)
    return Series, namesofSeries


def get_and_normalize(data, feat, stations):
    Series, namesofSeries = get_series(data, feat, stations)
    for i in range(len(Series)):
        scaler = MinMaxScaler()
        Series[i] = MinMaxScaler().fit_transform(Series[i].values.reshape(-1, 1))
        Series[i] = Series[i].reshape(len(Series[i]))
    return Series, namesofSeries


def plot_clusters(cluster_count, Series, labels):
    plot_count = math.ceil(math.sqrt(cluster_count))
    fig, axs = plt.subplots(plot_count, plot_count, figsize=(25, 25))
    fig.suptitle("Clusters")
    row_i = 0
    column_j = 0
    # For each label there is,
    # plots every series with that label
    for label in set(labels):
        cluster = []
        for i in range(len(labels)):
            if labels[i] == label:
                axs[row_i, column_j].plot(Series[i], c="gray", alpha=0.4)
                cluster.append(Series[i])
        if len(cluster) > 0:
            axs[row_i, column_j].plot(np.average(np.vstack(cluster), axis=0), c="red")
        axs[row_i, column_j].set_title("Cluster " + str(label))
        column_j += 1
        if column_j % plot_count == 0:
            row_i += 1
            column_j = 0

    plt.show()
