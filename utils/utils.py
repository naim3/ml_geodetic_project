import pandas as pd 
import numpy as np 
import matplotlib.pyplot as plt 

def plot_series_hist(Series, namesofSeries):
    fig, axs = plt.subplots(2, 3, figsize=(25, 12))
    i = 0 
    for j in range(3):
        if i * 3 + j + 1 > len(Series):  # pass the others that we can't fill
            continue
        axs[i, j].plot(Series[i * 3 + j])
        axs[i, j].set_title(namesofSeries[i * 3 + j])
    i = 1
    for j in range(3):
            if i * 3 + j + 1 > len(Series):  # pass the others that we can't fill
                continue
            axs[i, j].hist(Series[i * 3 + j], bins=10)
            axs[i, j].set_title(namesofSeries[i * 3 + j])    
            
    plt.show()
