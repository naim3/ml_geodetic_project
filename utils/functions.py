import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt 
from neuralprophet import NeuralProphet
from statsmodels.tsa.stattools import adfuller
from scipy import stats


def correlation_plot(data,columns=None):
  if columns!=None:
    data=data[columns]
  f, ax = plt.subplots(figsize= [10,8])
  sns.heatmap(data.corr(), annot=True, fmt=".2f", ax=ax, 
              cbar_kws={'label': 'Correlation Coefficient'}, cmap='viridis')
  ax.set_title("Correlation Matrix for different strains ", fontsize=18)
  plt.show()

def Stationnarity_Test(data):
  X = data.Final_Strain
  result = adfuller(X)
  print('ADF Statistic: %f' % result[0])
  print('p-value: %f' % result[1])

def Normality_Test(data):
  X = data.Final_Strain
  jarque_bera_test = stats.jarque_bera(X)
  print('jarque_bera_test Statistic: %f' % jarque_bera_test[0])
  print('p-value: %f' % jarque_bera_test[1])


def neuralprophet_analysis(data):
  dict={"areal":areal,"shear":shear,"differential":differential}
  df = dict[data][["Final_Strain"]].copy()
  df["date"]=areal.index
  df.rename(columns={"date": "ds", "Final_Strain": "y"}, inplace=True)
  model = NeuralProphet()
  metrics = model.fit(df, validate_each_epoch=True, freq="5min") 
  print(f'\n Results for {data} strain:')
  model.plot_parameters()
  return model
