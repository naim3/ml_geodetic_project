import shutil
import gzip
import os
import tarfile
import urllib.request as request
from contextlib import closing
import pathlib
def main(stations,years):
    for station in stations:
        try:
            print(station)
            for year in years:
                try:
                    with closing(request.urlopen(f'ftp://bsm.unavco.org/pub/bsm/level2/{station}/{station}.{year}.bsm.level2.tar')) as r:
                        print(year)
                        with open('/home/amine/University_Project/ml_geodetic_project/Data/file', 'wb') as f:
                            shutil.copyfileobj(r, f)
                        my_tar = tarfile.open('/home/amine/University_Project/ml_geodetic_project/Data/file')
                        my_tar.extractall('/home/amine/University_Project/ml_geodetic_project/Data/') # specify which folder to extract to
                        my_tar.close()   
                    for strain in ['2Ene','Eee+Enn','Eee-Enn']:
                        with gzip.open(f'/home/amine/University_Project/ml_geodetic_project/Data/{station}.{year}.bsm.level2/{station}.{year}.xml.{strain}.txt.gz', 'rb') as f_in:
                            if not os.path.exists(f'/home/amine/University_Project/ml_geodetic_project/Data/{station}/{year}'):
                                os.makedirs(f'/home/amine/University_Project/ml_geodetic_project/Data/{station}/{year}')
                            with open(f'/home/amine/University_Project/ml_geodetic_project/Data/{station}/{year}/{station}.{year}.xml.{strain}.txt', 'wb') as f_out:
                                shutil.copyfileobj(f_in, f_out)
                    shutil.rmtree(f'/home/amine/University_Project/ml_geodetic_project/Data/{station}.{year}.bsm.level2') 
                except:
                    continue
            with closing(request.urlopen(f'ftp://bsm.unavco.org/pub/bsm/level2/{station}/{station}.README.txt')) as r:
                    with open(f'/home/amine/University_Project/ml_geodetic_project/Data/{station}/{station}.README.txt', 'wb') as f:
                        shutil.copyfileobj(r, f)
                
        except:
            continue
            
if __name__ == "__main__":
    
    stations = [f'B{i}{j}{k}' for i in range(0,10) for j in range(0,10) for k in range(0,10)]
    years = [str(i) for i in range(1999,2022)]
    main(stations,years)