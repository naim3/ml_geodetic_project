import pandas as pd


def get_locations():
    lat= []
    long =[]
    elevation = []
    stations = []
    for station in [f'B{i}{j}{k}' for i in range(0,10) for j in range(0,10) for k in range(0,10)]:
        try:
            with open(f"/home/amine/University_Project/ml_geodetic_project/Data/{station}/{station}.README.txt") as f:
                lines = f.readlines()
            lat.append(float(lines[3].split(' ')[2].split('\t')[0]))
            long.append(float(lines[3].split(' ')[4]))
            elevation.append(float(lines[3].split(' ')[-3]))
            stations.append(station)
        except:
            continue
    df = pd.DataFrame(columns=['Station','Latitude','Longitude','Elevation'])
    df.Station=stations
    df.Latitude=lat
    df.Longitude=long
    df.Elevation=elevation
    df.to_csv('/home/amine/University_Project/ml_geodetic_project/Data/Locations.csv',index=False)