import os
import musdb
import stempeg
import soundfile
from tqdm.notebook import tqdm

import h5py
import numpy as np
from sortedcontainers import SortedList
import librosa

TARGETS = {"mixture": 0, "drums": 1, "bass": 2, "other": 3, "vocals": 4}


def write_wav(path, audio, sr):
    soundfile.write(path, audio.T, sr, "PCM_16")


def get_musdb(database_path, verbose):
    mus = musdb.DB(root=database_path, is_wav=False)
    subsets = list()
    for subset in ["train", "test"]:
        tracks = mus.load_mus_tracks(subset)
        samples = list()
        # Go through tracks
        for track in tqdm(tracks):
            # Skip track if mixture is already written, assuming this track is done already
            track_path = track.path[:-9]
            mix_path = track_path + "_mix.wav"
            acc_path = track_path + "_accompaniment.wav"
            if os.path.exists(mix_path):
                if verbose:
                    print(
                        "WARNING: Skipping track "
                        + mix_path
                        + " since it exists already"
                    )
                # Add paths and then skip
                paths = {"mix": mix_path, "accompaniment": acc_path}
                paths.update(
                    {
                        key: track_path + "_" + key + ".wav"
                        for key in ["bass", "drums", "other", "vocals"]
                    }
                )
                samples.append(paths)
                continue
            rate = track.rate
            # Go through each instrument
            paths = dict()
            stem_audio = dict()
            for stem in ["bass", "drums", "other", "vocals"]:
                path = track_path + "_" + stem + ".wav"
                audio, _ = stempeg.read_stems(track.path, stem_id=[TARGETS[stem]])
                audio = audio[:, 0]
                write_wav(path, audio, rate)
                stem_audio[stem] = audio
                paths[stem] = path
            # Add other instruments to form accompaniment
            acc_audio = np.clip(
                sum(
                    [
                        stem_audio[key]
                        for key in list(stem_audio.keys())
                        if key != "vocals"
                    ]
                ),
                -1.0,
                1.0,
            )
            write_wav(acc_path, acc_audio, rate)
            paths["accompaniment"] = acc_path
            # Create mixture
            mix_audio, _ = stempeg.read_stems(track.path, stem_id=[TARGETS["mixture"]])
            mix_audio = mix_audio[:, 0]
            write_wav(mix_path, mix_audio, rate)
            paths["mix"] = mix_path
            diff_signal = np.abs(mix_audio - acc_audio - stem_audio["vocals"])
            if verbose:
                print(
                    "Maximum absolute deviation from source additivity constraint: "
                    + str(np.max(diff_signal))
                )  # Check if acc+vocals=mix
                print(
                    "Mean absolute deviation from source additivity constraint:    "
                    + str(np.mean(diff_signal))
                )
            samples.append(paths)
        subsets.append(samples)
    print("DONE preparing dataset!")
    return subsets


def get_musdb_folds(root_path):
    dataset = get_musdb(root_path, False)
    train_val_list = dataset[0]
    test_list = dataset[1]

    np.random.seed(1337)  # Ensure that partitioning is always the same on each run
    train_list = np.random.choice(train_val_list, 75, replace=False)
    val_list = [elem for elem in train_val_list if elem not in train_list]
    # print("First training song: " + str(train_list[0])) # To debug whether partitioning is deterministic
    return {"train": train_list, "val": val_list, "test": test_list}


def load(path, sr=22050, mono=True, mode="numpy", offset=0.0, duration=None):
    y, curr_sr = librosa.load(
        path, sr=sr, mono=mono, res_type="kaiser_fast", offset=offset, duration=duration
    )

    if len(y.shape) == 1:
        # Expand channel dimension
        y = y[np.newaxis, :]

    if mode == "pytorch":
        y = torch.tensor(y)

    return y, curr_sr


class SeparationDataset:
    def __init__(
        self,
        dataset,
        partition,
        instruments,
        sr,
        channels,
        shapes,
        random_hops,
        hdf_dir,
        in_memory=False,
        audio_transform=None,
        std=None, 
        
    ):

        self.hdf_dataset = None
        os.makedirs(hdf_dir, exist_ok=True)
        self.hdf_dir = os.path.join(hdf_dir, partition + ".hdf5")
        self.std = None
        self.random_hops = random_hops
        self.sr = sr
        self.channels = channels
        self.shapes = shapes
        self.in_memory = in_memory
        self.instruments = instruments
        self.audio_transform = audio_transform
        # PREPARE HDF FILE

        # Check if HDF file exists already
        if not os.path.exists(self.hdf_dir):
            # Create folder if it did not exist before
            if not os.path.exists(hdf_dir):
                os.makedirs(hdf_dir)

            # Create HDF file
            with h5py.File(self.hdf_dir, "w") as f:
                f.attrs["sr"] = sr
                f.attrs["channels"] = channels
                f.attrs["instruments"] = instruments

                print("Adding audio files to dataset (preprocessing)...")
                for idx, example in enumerate(tqdm(dataset[partition])):
                    # Load mix
                    mix_audio, _ = load(
                        example["mix"], sr=self.sr, mono=(self.channels == 1)
                    )

                    source_audios = []
                    for source in instruments:
                        # In this case, read in audio and convert to target sampling rate
                        source_audio, _ = load(
                            example[source], sr=self.sr, mono=(self.channels == 1)
                        )
                        source_audios.append(source_audio)
                    source_audios = np.concatenate(source_audios, axis=0)
                    assert source_audios.shape[1] == mix_audio.shape[1]

                    # Add to HDF5 file
                    grp = f.create_group(str(idx))
                    grp.create_dataset(
                        "inputs",
                        shape=mix_audio.shape,
                        dtype=mix_audio.dtype,
                        data=mix_audio,
                    )
                    grp.create_dataset(
                        "targets",
                        shape=source_audios.shape,
                        dtype=source_audios.dtype,
                        data=source_audios,
                    )
                    grp.attrs["length"] = mix_audio.shape[1]
                    grp.attrs["target_length"] = source_audios.shape[1]

        # In that case, check whether sr and channels are complying with the audio in the HDF file, otherwise raise error
        with h5py.File(self.hdf_dir, "r") as f:
            if (
                f.attrs["sr"] != sr
                or f.attrs["channels"] != channels
                or list(f.attrs["instruments"]) != instruments
            ):
                raise ValueError(
                    "Tried to load existing HDF file, but sampling rate and channel or instruments are not as expected. Did you load an out-dated HDF file?"
                )

        # HDF FILE READY

        # SET SAMPLING POSITIONS

        # Go through HDF and collect lengths of all audio files
        with h5py.File(self.hdf_dir, "r") as f:
            lengths = [
                f[str(song_idx)].attrs["target_length"] for song_idx in range(len(f))
            ]

            # Subtract input_size from lengths and divide by hop size to determine number of starting positions
            lengths = [(l // self.shapes["output_frames"]) + 1 for l in lengths]

        self.start_pos = SortedList(np.cumsum(lengths))
        self.length = self.start_pos[-1]

    def __len__(self):
        return self.length

    def __getitem__(self, index):
        # Open HDF5
        if self.hdf_dataset is None:
            driver = (
                "core" if self.in_memory else None
            )  # Load HDF5 fully into memory if desired
            self.hdf_dataset = h5py.File(self.hdf_dir, "r", driver=driver)

        # Find out which slice of targets we want to read
        audio_idx = self.start_pos.bisect_right(index)
        if audio_idx > 0:
            index = index - self.start_pos[audio_idx - 1]

        # Check length of audio signal
        audio_length = self.hdf_dataset[str(audio_idx)].attrs["length"]
        target_length = self.hdf_dataset[str(audio_idx)].attrs["target_length"]

        # Determine position where to start targets
        if self.random_hops:
            start_target_pos = np.random.randint(
                0, max(target_length - self.shapes["output_frames"] + 1, 1)
            )
        else:
            # Map item index to sample position within song
            start_target_pos = index * self.shapes["output_frames"]

        # READ INPUTS
        # Check front padding
        start_pos = start_target_pos - self.shapes["output_start_frame"]
        if start_pos < 0:
            # Pad manually since audio signal was too short
            pad_front = abs(start_pos)
            start_pos = 0
        else:
            pad_front = 0

        # Check back padding
        end_pos = (
            start_target_pos
            - self.shapes["output_start_frame"]
            + self.shapes["input_frames"]
        )
        if end_pos > audio_length:
            # Pad manually since audio signal was too short
            pad_back = end_pos - audio_length
            end_pos = audio_length
        else:
            pad_back = 0

        # Read and return
        audio = self.hdf_dataset[str(audio_idx)]["inputs"][:, start_pos:end_pos].astype(
            np.float32
        )
        if pad_front > 0 or pad_back > 0:
            audio = np.pad(
                audio,
                [(0, 0), (pad_front, pad_back)],
                mode="constant",
                constant_values=0.0,
            )

        targets = self.hdf_dataset[str(audio_idx)]["targets"][
            :, start_pos:end_pos
        ].astype(np.float32)
        if pad_front > 0 or pad_back > 0:
            targets = np.pad(
                targets,
                [(0, 0), (pad_front, pad_back)],
                mode="constant",
                constant_values=0.0,
            )

        out = {
            inst: targets[idx * self.channels : (idx + 1) * self.channels]
            for idx, inst in enumerate(self.instruments)
        }

        if hasattr(self, "audio_transform") and self.audio_transform is not None:
            audio, targets = self.audio_transform(audio, out)

        out["inputs"] = audio
        return out


class SSLSeparationDataset(SeparationDataset):
    def __init__(
        self,
        dataset,
        partition,
        instruments,
        sr,
        channels,
        shapes,
        random_hops,
        hdf_dir,
        in_memory=False,
        audio_transform=None,
        std=None,
        is_supervised = False ,
    ):

        super(SSLSeparationDataset, self).__init__(
            dataset,
            partition,
            instruments,
            sr,
            channels,
            shapes,
            random_hops,
            hdf_dir,
            in_memory,
            audio_transform,
            std,
        )
        self.is_supervised = is_supervised 

    def __len__(self):
        if self.is_supervised : 
            return self.length 
        
        return self.length * 20

    def __getitem__(self, index):
        # Open HDF5
        original_index = index
        index = index % self.length
        if self.hdf_dataset is None:
            driver = (
                "core" if self.in_memory else None
            )  # Load HDF5 fully into memory if desired
            self.hdf_dataset = h5py.File(self.hdf_dir, "r", driver=driver)

        # Find out which slice of targets we want to read
        audio_idx = self.start_pos.bisect_right(index)

        target_index = np.random.randint(0, self.length)
        try:
            target_idx = self.start_pos.bisect_right(target_index)
        except:
            target_index = index
            target_idx = self.start_pos.bisect_right(target_index)

        if audio_idx > 0:
            index = index - self.start_pos[audio_idx - 1]
            target_index = target_index - self.start_pos[target_idx - 1]

        # Check length of audio signal
        audio_length = self.hdf_dataset[str(audio_idx)].attrs["length"]
        target_length = self.hdf_dataset[str(audio_idx)].attrs["target_length"]

        # Determine position where to start targets
        if self.random_hops:
            start_target_pos = np.random.randint(
                0, max(target_length - self.shapes["output_frames"] + 1, 1)
            )
        else:
            # Map item index to sample position within song
            start_target_pos = index * self.shapes["output_frames"]

        # READ INPUTS
        # Check front padding
        start_pos = start_target_pos - self.shapes["output_start_frame"]
        if start_pos < 0:
            # Pad manually since audio signal was too short
            pad_front = abs(start_pos)
            start_pos = 0
        else:
            pad_front = 0

        # Check back padding
        end_pos = (
            start_target_pos
            - self.shapes["output_start_frame"]
            + self.shapes["input_frames"]
        )
        if end_pos > audio_length:
            # Pad manually since audio signal was too short
            pad_back = end_pos - audio_length
            end_pos = audio_length
        else:
            pad_back = 0

        targets = self.hdf_dataset[str(audio_idx)]["targets"][
            :, start_pos:end_pos
        ].astype(np.float32)
        if pad_front > 0 or pad_back > 0:
            targets = np.pad(
                targets,
                [(0, 0), (pad_front, pad_back)],
                mode="constant",
                constant_values=0.0,
            )
        out = {
            inst: targets[idx * self.channels : (idx + 1) * self.channels]
            for idx, inst in enumerate(self.instruments)
        }

        audio = out[self.instruments[0]] + out[self.instruments[1]]
        noise = self.hdf_dataset[str(target_idx)]["targets"][
            :, start_pos:end_pos
        ].astype(np.float32)
        if pad_front > 0 or pad_back > 0:
            noise = np.pad(
                noise,
                [(0, 0), (pad_front, pad_back)],
                mode="constant",
                constant_values=0.0,
            )
        out_noise = {
            inst: noise[idx * self.channels : (idx + 1) * self.channels]
            for idx, inst in enumerate(self.instruments)
        }
        try: 
            if self.is_supervised : 
                target = out[self.instruments[0]] 
            else : 
                target = out[self.instruments[0]] + out_noise[self.instruments[1]]
        except:
            target = out[self.instruments[0]]
        out = dict()
        out["target"] = target
        if hasattr(self, "audio_transform") and self.audio_transform is not None:
            audio, targets = self.audio_transform(audio, out)
        out["inputs"] = audio
        return out


class NoisyDataset(SeparationDataset):
    def __init__(
        self,
        dataset,
        partition,
        instruments,
        std,
        sr,
        channels,
        shapes,
        random_hops,
        hdf_dir,
        in_memory=False,
        audio_transform=None,
    ):

        super(NoisyDataset, self).__init__(
            dataset,
            partition,
            instruments,
            sr,
            channels,
            shapes,
            random_hops,
            hdf_dir,
            in_memory,
            audio_transform,
            std,
        )
        self.std = std

    def __len__(self):
        return self.length * 20

    def __getitem__(self, index):
        # Open HDF5
        original_index = index
        index = index % self.length
        if self.hdf_dataset is None:
            driver = (
                "core" if self.in_memory else None
            )  # Load HDF5 fully into memory if desired
            self.hdf_dataset = h5py.File(self.hdf_dir, "r", driver=driver)

        # Find out which slice of targets we want to read
        audio_idx = self.start_pos.bisect_right(index)
        if audio_idx > 0:
            index = index - self.start_pos[audio_idx - 1]

        # Check length of audio signal
        audio_length = self.hdf_dataset[str(audio_idx)].attrs["length"]
        target_length = self.hdf_dataset[str(audio_idx)].attrs["target_length"]

        # Determine position where to start targets
        if self.random_hops:
            start_target_pos = np.random.randint(
                0, max(target_length - self.shapes["output_frames"] + 1, 1)
            )
        else:
            # Map item index to sample position within song
            start_target_pos = index * self.shapes["output_frames"]

        # READ INPUTS
        # Check front padding
        start_pos = start_target_pos - self.shapes["output_start_frame"]
        if start_pos < 0:
            # Pad manually since audio signal was too short
            pad_front = abs(start_pos)
            start_pos = 0
        else:
            pad_front = 0

        # Check back padding
        end_pos = (
            start_target_pos
            - self.shapes["output_start_frame"]
            + self.shapes["input_frames"]
        )
        if end_pos > audio_length:
            # Pad manually since audio signal was too short
            pad_back = end_pos - audio_length
            end_pos = audio_length
        else:
            pad_back = 0

        # Read and return
        audio = self.hdf_dataset[str(audio_idx)]["inputs"][:, start_pos:end_pos].astype(
            np.float32
        )

        np.random.seed(index)
        noise = np.random.normal(0, self.std, audio.shape)

        audio = audio + noise
        if pad_front > 0 or pad_back > 0:
            audio = np.pad(
                audio,
                [(0, 0), (pad_front, pad_back)],
                mode="constant",
                constant_values=0.0,
            )

        targets = self.hdf_dataset[str(audio_idx)]["targets"][
            :, start_pos:end_pos
        ].astype(np.float32)

        np.random.seed(original_index)
        noise = np.random.normal(0, self.std, targets.shape)

        targets = targets + noise

        if pad_front > 0 or pad_back > 0:
            targets = np.pad(
                targets + noise,
                [(0, 0), (pad_front, pad_back)],
                mode="constant",
                constant_values=0.0,
            )

        out = {
            inst: targets[idx * self.channels : (idx + 1) * self.channels]
            for idx, inst in enumerate(self.instruments)
        }

        if hasattr(self, "audio_transform") and self.audio_transform is not None:
            audio, targets = self.audio_transform(audio, out)

        out["inputs"] = audio
        return out


class Noise2CleanDataset(SeparationDataset):
    def __init__(
        self,
        dataset,
        partition,
        instruments,
        std,
        sr,
        channels,
        shapes,
        random_hops,
        hdf_dir,
        in_memory=False,
        audio_transform=None,
    ):

        super(Noise2CleanDataset, self).__init__(
            dataset,
            partition,
            instruments,
            sr,
            channels,
            shapes,
            random_hops,
            hdf_dir,
            in_memory,
            audio_transform,
            std,
        )
        self.std = std

    def __len__(self):
        return self.length

    def __getitem__(self, index):
        # Open HDF5
        original_index = index
        index = index % self.length
        if self.hdf_dataset is None:
            driver = (
                "core" if self.in_memory else None
            )  # Load HDF5 fully into memory if desired
            self.hdf_dataset = h5py.File(self.hdf_dir, "r", driver=driver)

        # Find out which slice of targets we want to read
        audio_idx = self.start_pos.bisect_right(index)
        if audio_idx > 0:
            index = index - self.start_pos[audio_idx - 1]

        # Check length of audio signal
        audio_length = self.hdf_dataset[str(audio_idx)].attrs["length"]
        target_length = self.hdf_dataset[str(audio_idx)].attrs["target_length"]

        # Determine position where to start targets
        if self.random_hops:
            start_target_pos = np.random.randint(
                0, max(target_length - self.shapes["output_frames"] + 1, 1)
            )
        else:
            # Map item index to sample position within song
            start_target_pos = index * self.shapes["output_frames"]

        # READ INPUTS
        # Check front padding
        start_pos = start_target_pos - self.shapes["output_start_frame"]
        if start_pos < 0:
            # Pad manually since audio signal was too short
            pad_front = abs(start_pos)
            start_pos = 0
        else:
            pad_front = 0

        # Check back padding
        end_pos = (
            start_target_pos
            - self.shapes["output_start_frame"]
            + self.shapes["input_frames"]
        )
        if end_pos > audio_length:
            # Pad manually since audio signal was too short
            pad_back = end_pos - audio_length
            end_pos = audio_length
        else:
            pad_back = 0

        # Read and return
        audio = self.hdf_dataset[str(audio_idx)]["inputs"][:, start_pos:end_pos].astype(
            np.float32
        )

        np.random.seed(index)
        noise = np.random.normal(0, self.std, audio.shape)

        audio = audio + noise
        if pad_front > 0 or pad_back > 0:
            audio = np.pad(
                audio,
                [(0, 0), (pad_front, pad_back)],
                mode="constant",
                constant_values=0.0,
            )

        targets = self.hdf_dataset[str(audio_idx)]["targets"][
            :, start_pos:end_pos
        ].astype(np.float32)

        if pad_front > 0 or pad_back > 0:
            targets = np.pad(
                targets + noise,
                [(0, 0), (pad_front, pad_back)],
                mode="constant",
                constant_values=0.0,
            )

        out = {
            inst: targets[idx * self.channels : (idx + 1) * self.channels]
            for idx, inst in enumerate(self.instruments)
        }

        if hasattr(self, "audio_transform") and self.audio_transform is not None:
            audio, targets = self.audio_transform(audio, out)

        out["inputs"] = audio
        return out
