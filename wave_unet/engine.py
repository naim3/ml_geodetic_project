import os
import time
from functools import partial

import torch
import pickle
import numpy as np

import torch.nn as nn
from torch.utils.tensorboard import SummaryWriter
from transformers import AdamW
from tqdm.notebook import tqdm

import model.utils as model_utils
from utils import *
from dataset import *


def compute_loss(model, inputs, targets, criterion, compute_grad=False):
    all_outputs = {}
    if model.module.separate:
        avg_loss = 0.0
        num_sources = 0
        for inst in model.module.instruments:
            output = model(inputs, inst)
            loss = criterion(output[inst], targets[inst])

            if compute_grad:
                loss.backward()

            avg_loss += loss.item()
            num_sources += 1
            all_outputs[inst] = output[inst].detach().clone()
        avg_loss /= float(num_sources)

    else:
        loss = 0
        all_outputs = model(inputs)
        for inst in all_outputs.keys():
            loss += criterion(all_outputs[inst], targets[inst])

        if compute_grad:
            loss.backward()

        avg_loss = loss.item() / float(len(all_outputs))

    return all_outputs, avg_loss


def compute_model_output(model, inputs):
    all_outputs = []
    if model.module.separate:
        for inst in model.module.instruments:
            output = model(inputs, inst)
            all_outputs[inst] = output[inst].detach().clone()
    else:
        all_outputs = model(inputs)

    return all_outputs


class AverageMeter:
    """
    Computes and stores the average and current value
    """

    def __init__(self):
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count


def train(model, dataloader, optimizer, criterion, device):

    model.train()
    tr_loss = 0
    counter = 0
    losses = AverageMeter()
    tk0 = tqdm(enumerate(dataloader), total=len(dataloader))

    for bi, d in tk0:
        inputs = d["inputs"].to(device, dtype=torch.float)
        targets = dict()
        for inst in model.module.instruments:
            targets[inst] = d[inst].to(device, dtype=torch.float)
        optimizer.zero_grad()
        outputs, avg_loss = compute_loss(
            model, inputs, targets, criterion, compute_grad=True
        )
        optimizer.step()

        losses.update(avg_loss, inputs.size(0))
        tk0.set_postfix(loss=losses.avg)
        tr_loss += avg_loss
        counter += 1

        if bi == 10:
            input_to_log = inputs[
                0,
                :,
                model.module.shapes["output_start_frame"] : model.module.shapes[
                    "output_end_frame"
                ],
            ]  # Stereo not supported for logs yet
            output_to_log = dict()
            target_to_log = dict()
            for inst in outputs.keys():
                output_to_log[inst] = outputs[inst][0]
                target_to_log[inst] = targets[inst][0]

    return tr_loss / counter, input_to_log, output_to_log, target_to_log


def valid(
    model, dataloader, criterion, device,
):

    model.eval()
    valid_loss = 0
    counter = 0
    losses = AverageMeter()
    tk0 = tqdm(enumerate(dataloader), total=len(dataloader))
    with torch.no_grad():
        for bi, d in tk0:
            inputs = d["inputs"].to(device, dtype=torch.float)
            targets = dict()
            for inst in model.module.instruments:
                targets[inst] = d[inst].to(device, dtype=torch.float)
            outputs, avg_loss = compute_loss(
                model, inputs, targets, criterion, compute_grad=False
            )
            losses.update(avg_loss, inputs.size(0))
            tk0.set_postfix(loss=losses.avg)
            valid_loss += avg_loss
            counter += 1
    return valid_loss / counter


def run(
    model,
    loss,
    train_data,
    val_data=None,
    instruments=None,
    sr=None,
    log_dir=None,
    save_path=None,
    params=None,
):

    train_dataloader = torch.utils.data.DataLoader(
        train_data, batch_size=params["batch_size"], shuffle=True, num_workers=16
    )
    if val_data is not None:
        valid_dataloader = torch.utils.data.DataLoader(
            val_data, batch_size=params["batch_size"], shuffle=True, num_workers=16
        )

    writer = SummaryWriter(log_dir)

    if loss == "L1":
        criterion = nn.L1Loss()
    elif loss == "L2":
        criterion = nn.MSELoss()
    else:
        raise NotImplementedError("couldn't find this loss!")

    optimizer = AdamW(params=model.parameters(), lr=params["lr"])
    scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(
        optimizer, factor=0.2, patience=3, verbose=True
    )

    state = {"epoch": 0, "patience": 0, "max_wait": 10, "best_loss": np.Inf}

    print("Training Start")

    for epoch in range(params["epochs"]):
        print(f"-------------{epoch}-------------")
        train_loss, input, output, target = train(
            model, train_dataloader, optimizer, criterion, params["device"]
        )
        writer.add_scalar("train_loss", train_loss, state["epoch"])
        writer.add_audio("input", input, state["epoch"], sample_rate=sr)
        for inst in output.keys():
            writer.add_audio(
                inst + "_pred", output[inst], state["epoch"], sample_rate=sr
            )
            writer.add_audio(
                inst + "_target", target[inst], state["epoch"], sample_rate=sr
            )

        if val_data is not None:
            val_loss = valid(model, valid_dataloader, criterion, params["device"])
            scheduler.step(val_loss)
            writer.add_scalar("val_loss", val_loss, state["epoch"])

        state["epoch"] += 1
        torch.save(model.state_dict(), save_path)

    model.load_state_dict(torch.load(save_path))
    return None
