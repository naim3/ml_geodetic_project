from tqdm.notebook import tqdm

import museval
from model.utils import *
from utils import *
from engine import *
from dataset import *


def predict(audio, model):
    if isinstance(audio, torch.Tensor):
        is_cuda = audio.is_cuda
        audio = audio.detach().cpu().numpy()
        return_mode = "pytorch"
    else:
        return_mode = "numpy"

    expected_outputs = audio.shape[1]

    output_shift = model.module.shapes["output_frames"]
    pad_back = audio.shape[1] % output_shift
    pad_back = 0 if pad_back == 0 else output_shift - pad_back
    if pad_back > 0:
        audio = np.pad(
            audio, [(0, 0), (0, pad_back)], mode="constant", constant_values=0.0
        )
    target_outputs = audio.shape[1]
    outputs = {
        key: np.zeros(audio.shape, np.float32) for key in model.module.instruments
    }

    # Pad mixture across time at beginning and end so that neural network can make prediction at the beginning and end of signal
    pad_front_context = model.module.shapes["output_start_frame"]
    pad_back_context = (
        model.module.shapes["input_frames"] - model.module.shapes["output_end_frame"]
    )
    audio = np.pad(
        audio,
        [(0, 0), (pad_front_context, pad_back_context)],
        mode="constant",
        constant_values=0.0,
    )
    # Iterate over mixture magnitudes, fetch network prediction
    with torch.no_grad():
        for target_start_pos in range(
            0, target_outputs, model.module.shapes["output_frames"]
        ):
            # Prepare mixture excerpt by selecting time interval
            curr_input = audio[
                :,
                target_start_pos : target_start_pos
                + model.module.shapes["input_frames"],
            ]  # Since audio was front-padded input of [targetpos:targetpos+inputframes] actually predicts [targetpos:targetpos+outputframes] target range

            # Convert to Pytorch tensor for model prediction
            curr_input = torch.from_numpy(curr_input).unsqueeze(0)

            # Predict
            for key, curr_targets in compute_model_output(model, curr_input).items():
                outputs[key][
                    :,
                    target_start_pos : target_start_pos
                    + model.module.shapes["output_frames"],
                ] = (curr_targets.squeeze(0).cpu().numpy())

    # Crop to expected length (since we padded to handle the frame shift)
    outputs = {key: outputs[key][:, :expected_outputs] for key in outputs.keys()}
    return outputs


def predict_song(audio_path, model):
    model.eval()
    mix_audio, mix_sr = load(audio_path, sr=None, mono=True)
    mix_channels = mix_audio.shape[0]
    mix_len = mix_audio.shape[1]

    mix_channels = mix_audio.shape[0]
    mix_len = mix_audio.shape[1]

    sources = predict(mix_audio, model)
    return sources


def evaluate(dataset, model, instruments):
    perfs = list()
    model.eval()
    with torch.no_grad():
        for example in tqdm(dataset):
            target_sources = np.stack(
                [
                    load(example[instrument], sr=None, mono=True)[0].T
                    for instrument in instruments
                ]
            )
            pred_sources = predict_song(example["mix"], model)
            pred_sources = np.stack(
                [pred_sources[instrument].T for instrument in instruments]
            )
            SDR, ISR, SIR, SAR, _ = museval.metrics.bss_eval(
                target_sources, pred_sources
            )
            song = {}
            for idx, name in enumerate(instruments):
                song[name] = {
                    "SDR": SDR[idx],
                    "ISR": ISR[idx],
                    "SIR": SIR[idx],
                    "SAR": SAR[idx],
                }
            perfs.append(song)

    return perfs


def predict_noised_song(audio_path, model, std, device):
    model.eval()
    mix_audio_, mix_sr = load(audio_path, sr=None, mono=True)
    noise_ = np.random.normal(0, std, mix_audio_.shape)
    noise = torch.tensor(noise_, dtype=torch.float).to(device)
    mix_audio = torch.tensor(mix_audio_, dtype=torch.float).to(device)
    sources = predict(mix_audio + noise, model)
    sources["noised"] = mix_audio_ + noise_
    return sources


def denoising_evaluate(dataset, model, instruments, std, device):
    perfs = list()
    model.eval()
    with torch.no_grad():
        for example in tqdm(dataset):
            target_sources = np.stack(
                [load(example["mix"], sr=None, mono=True)[0].T for _ in range(2)]
            )
            pred_sources_ = predict_noised_song(example["mix"], model, std, device)
            pred_sources = np.stack(
                [pred_sources_[instrument].T for instrument in pred_sources_.keys()]
            )
            SDR, ISR, SIR, SAR, _ = museval.metrics.bss_eval(
                target_sources, pred_sources
            )
            song = {}
            for idx, name in enumerate(pred_sources_.keys()):
                song[name] = {
                    "SDR": SDR[idx],
                    "ISR": ISR[idx],
                    "SIR": SIR[idx],
                    "SAR": SAR[idx],
                }
            perfs.append(song)

    return perfs


def predict_ssl(vocals_path, drums_path, model, device):
    model.eval()
    vocals, _ = load(vocals_path, sr=None, mono=True)
    drums, _ = load(drums_path, sr=None, mono=True)
    mix_audio = torch.tensor(vocals + drums, dtype=torch.float).to(device)
    sources = predict(mix_audio, model)
    sources["mix"] = vocals + drums
    return sources


def evaluate_ssl_sourceseparation(dataset, model, instruments, device):
    perfs = list()
    model.eval()
    with torch.no_grad():
        for example in tqdm(dataset):
            target_sources = np.stack(
                [
                    load(example[instruments[0]], sr=None, mono=True)[0].T
                    for _ in range(2)
                ]
            )
            pred_sources = predict_ssl(
                example[instruments[0]], example[instruments[1]], model, device
            )
            pred_sources = np.stack(
                [pred_sources[instrument].T for instrument in ["target", "mix"]]
            )
            SDR, _, _, _, _ = museval.metrics.bss_eval(target_sources, pred_sources)
            song = {}
            for idx, name in enumerate(["predicted_vocals", "mixed_source"]):
                song[name] = {"SDR": SDR[idx]}
            perfs.append(song)
    return perfs
