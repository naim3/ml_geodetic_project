import matplotlib.pyplot as plt 
from tqdm.notebook import tqdm

import museval
import librosa
import numpy as np
import soundfile
import torch

from engine import *


def random_amplify(mix, targets, shapes, min, max):

    residual = mix  # start with original mix
    for key in targets.keys():
        if key != "mix":
            residual -= targets[
                key
            ]  # subtract all instruments (output is zero if all instruments add to mix)
    mix = residual * np.random.uniform(
        min, max
    )  # also apply gain data augmentation to residual
    for key in targets.keys():
        if key != "mix":
            targets[key] = targets[key] * np.random.uniform(min, max)
            mix += targets[key]  # add instrument with gain data augmentation to mix
    mix = np.clip(mix, -1.0, 1.0)
    return crop_targets(mix, targets, shapes)


def crop_targets(mix, targets, shapes):
    """
    Crops target audio to the output shape required by the model given in "shapes"
    """
    for key in targets.keys():
        targets[key] = targets[key][
            :, shapes["output_start_frame"] : shapes["output_end_frame"]
        ]
    return mix, targets


def resample(audio, orig_sr, new_sr, mode="numpy"):
    if orig_sr == new_sr:
        return audio

    if isinstance(audio, torch.Tensor):
        audio = audio.detach().cpu().numpy()

    out = librosa.resample(audio, orig_sr, new_sr, res_type="kaiser_fast")

    if mode == "pytorch":
        out = torch.tensor(out)
    return out

def plot(signal , sr , title) : 
    plt.figure(1, figsize=(15, 8))
    # Plot the signal
    plt.subplot(211)
    plt.title(f'{title}')
    plt.plot(signal)
    plt.xlabel('Sample')
    plt.ylabel('Amplitude')
    # Plot the spectrogram
    plt.subplot(212)
    powerSpectrum, freqenciesFound, time, imageAxis = plt.specgram(signal, Fs=sr)
    plt.xlabel('Time')
    plt.ylabel('Frequency')
    plt.show()  