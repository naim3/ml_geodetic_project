import museval
from functools import partial
import pandas as pd
import numpy as np

class Evaluation:
    def __init__(self,data,songs_names,model):
        self.data = data
        self.songs_names=songs_names
        self.model = model
        
    def get_song(self,x,name):
        if name in x:
            return  1
        else:
            return 0
        
    def sort(self,x):
        return int(x.split('_')[-1].split('.')[0])

    def construct_song_df(self,song_name):
        get_song_func = partial(self.get_song,name=song_name)
        df = self.data.copy()
        df['song'] = df['chunk_path'].apply(get_song_func)
        df=df[df['song']==1].copy()
        df['chunk_number']=df['chunk_path'].apply(self.sort)
        df.sort_values('chunk_number',inplace=True, ascending=True)
        return df
    
    def calculate_metrics_one_song(self,song_name):
        self.model.model.eval()
        df = self.construct_song_df(song_name)
        predictions, target, _ = self.model.predict(df)
        target = target.reshape(1,target.shape[2],1)
        predictions =predictions.reshape(1,predictions.shape[2],1)
        SDR, ISR, SIR, SAR, _ = museval.metrics.bss_eval(
                target, predictions,window=np.inf
            )
        return SDR, ISR, SIR, SAR
        
    def calculate_metrics(self):
        metrics = dict()
        for song_name in self.songs_names:
            try:
                SDR, ISR, SIR, SAR  = self.calculate_metrics_one_song(song_name)
                metrics[song_name] = {
                    "SDR": SDR.item(),
                    "ISR": ISR.item(),
                    "SIR": SIR.item(),
                    "SAR": SAR.item(),
                }
            except:
                continue
                
        for metric in ['SDR','ISR','SIR','SAR']:
            try:
                mean = np.mean([metrics[i][metric][0] for i in self.songs_names])
                metrics[f'mean_{metric}'] = mean
            except:
                continue
                
        return metrics