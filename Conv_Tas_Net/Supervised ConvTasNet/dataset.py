import torch
import numpy as np

TARGETS = { 'mixture':0,
            'drums':1, 
            'bass':2, 
            'rest':3, 
            'vocals':4 }
class NoiseSepDataset() : 
    def __init__(self, data ,
                 mus_path = '/SSD/University/',
                 input_size = 1000 , output_size = 1000 ,instruments =  ['drums', 'bass', 'rest', 'vocals']) : 
        self.data = data 
        self.mus_path = mus_path + self.data.chunk_path.values
        self.input_size = input_size 
        self.output_size = output_size 
        self.instruments = instruments 
    def __len__(self) : 
        return len(self.data) 
    def _pad_input(self , x ) : 
        size = x.shape[0]
        pad_size = self.input_size - size 
        pad_tensor = torch.tensor([0]*pad_size , dtype =torch.float) 
        pad_tensor = torch.cat([x , pad_tensor] , dim = 0) 
        return pad_tensor.unsqueeze(0)
    def _pad_output(self , x ):
        size = x.shape[0]
        pad_size = self.output_size - size 
        pad_tensor = torch.tensor([0]*pad_size , dtype =torch.float) 
        pad_tensor = torch.cat([x , pad_tensor] , dim = 0) 
        return pad_tensor.unsqueeze(0)
    def __getitem__(self , item) : 
        out = dict() 
        signal = np.load(self.mus_path[item])
        x = torch.tensor(signal[TARGETS['mixture'],:] , dtype = torch.float) 
        out['input'] = self._pad_input(x) 
        target = []
        for inst in self.instruments  : 
            x = torch.tensor(signal[TARGETS[inst],:] , dtype = torch.float)  
            x = self._pad_output(x)
            target.append(x) 
        out['target'] = torch.cat(target, dim = 0)    
        return out